﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIORIENTATION
{
    public partial class Orientation : Form
    {
        public Orientation()
        {
            InitializeComponent();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void aProposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Application SIORIENTATION Version 0.1");
        }

        private void démarrerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
